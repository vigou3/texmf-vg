%%    Modifications à la classe exam pour mes examens.
%%
%%    Nouvelles commandes:
%%
%%    \coursenumber{}   numéro du cours (obligatoire)
%%    \exam{}           examen (ogligatoire)
%%    \date{}           date (ogligatoire)
%%    \duration{}       durée (ogligatoire)
%%
%%    Préparé par Vincent Goulet 9 mars 2014
%%    Révisé par Vincent Goulet mars 2016
%%      pour utilisation de Lucida Grande Mono DK comme
%%      police non proportionnelle
%%    Révisé par Vincent Goulet novembre 2018
%%      pour conformité avec les nouvelles normes de
%%      présentation visuelle de l'UL (logo et police
%%      Overpass)
%%    Révisé par Vincent Goulet en octobre 2020
%%      pour ajouter des emplacements pour le nom et le NI
%%      sur la couverture pour cause de COVID-19

\NeedsTeXFormat{LaTeX2e}[1995/12/01]
\ProvidesClass{vgexam}[2016/03/07 (Vincent Goulet)]
\RequirePackage{ifthen}
\RequirePackage{ifxetex}
\newboolean{VG@doc}
\newboolean{VG@nodoc}
\setboolean{VG@doc}{false}
\DeclareOption{nodoc}{\setboolean{VG@doc}{false}}
\DeclareOption{doc}{\setboolean{VG@doc}{true}}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{exam}}
\ProcessOptions
\LoadClass[11pt,addpoints]{exam}

\RequirePackage{ifthen}
\RequirePackage[french]{babel}
\RequirePackage[autolanguage]{numprint}
\RequirePackage{amsmath}
\RequirePackage[noae]{Sweave}
\RequirePackage{graphicx}
\RequirePackage[absolute]{textpos}

%% Polices de caractères.
\ifxetex
  \RequirePackage{fontspec}
  \RequirePackage{unicode-math}
  \defaultfontfeatures{Scale=0.92,Ligatures=TeX}
  \setmainfont{Lucida Bright OT}
  \setmathfont{Lucida Bright Math OT}
  \setmonofont{Lucida Grande Mono DK}
  \setsansfont[BoldFont = {Overpass SemiBold}]{Overpass Regular}
\else
  \RequirePackage[T1]{fontenc}
  \RequirePackage[utf8]{inputenc}
  \RequirePackage{lucidabr}
  \RequirePackage[scaled=0.92]{beramono}
\fi
\RequirePackage[babel=true]{microtype}
\RequirePackage{icomma}

%% Commandes additionnelles pour spécifier le numéro de cours et al.
\newcommand{\VG@coursenumber}{}
\newcommand{\VG@exam}{}
\newcommand{\VG@duration}{}
\newcommand{\coursenumber}[1]{%
  \renewcommand{\VG@coursenumber}{\MakeUppercase{#1}}}
\newcommand{\exam}[1]{\renewcommand{\VG@exam}{#1}}
\newcommand{\duration}[1]{\renewcommand{\VG@duration}{#1}}

%% Commande pour ajouter des pages additionnelles en fin de cahier
\newcommand{\extrablankpage}{%
  \newpage
  \extraheadheight{-.1in}
  \header{}{\sffamily\small ESPACE ADDITIONNEL}{}
  \vspace*{\stretch{1}}}

%% Options de babel
\frenchbsetup{CompactItemize=false,%
  ThinSpaceInFrenchNumbers=true,
  og=«, fg=»}
\addto\captionsfrench{\def\figurename{{\scshape Fig.}}}
\addto\captionsfrench{\def\tablename{{\scshape Tab.}}}

%% Positionnement du logo
\TPGrid{8}{64}
\newlength{\logoheight}
\newlength{\logowidth}
\setlength{\logoheight}{4\TPVertModule}
\setlength{\logowidth}{4.727273\logoheight}

%% Redéfinition de la commande \maketitle pour créer la page titre
\renewcommand{\maketitle}{%
  \begin{coverpages}
    \flushleft\sffamily%
    \pagestyle{empty}
    \begin{textblock*}{165mm}(25mm,15mm)
      \includegraphics[height=\logoheight,keepaspectratio]{UL_FSG_Actuariat_N_d-2L}
    \end{textblock*}
    \vspace*{25mm}
    \begin{center}
      \renewcommand{\arraystretch}{1.2}
      \begin{tabular*}{115mm}{@{}p{70mm}p{35mm}@{}}
        \textbf{Cours} \newline \VG@coursenumber &
        \textbf{Examen} \newline \VG@exam \\
        \textbf{Professeur} \newline Vincent Goulet &
        \textbf{Date} \newline \@date \\
        \textbf{Nombre total de pages} \newline \numpages &
        \textbf{Durée} \newline \VG@duration \\
      \end{tabular*}\par
      \vspace{20mm}
      \begin{minipage}{115mm}
        \setlength{\fboxsep}{2mm}
        NOM\hfill  \fbox{\strut\hspace{100mm}} \\[5mm]
        NUMÉRO D'IDENTIFICATION (NI)\hfill \fbox{\strut\hspace{55mm}}
      \end{minipage}\par
      \vspace{20mm}
      \ifprintanswers
        {\large \textbf{SOLUTIONS}} \\[\baselineskip]
        \textbf{À L'USAGE EXCLUSIF DE L'AUXILIAIRE D'ENSEIGNEMENT}
        \vfill
      \else
        INSTRUCTIONS \par
        \vspace{10mm}
        \begin{minipage}{115mm}
          \begin{enumerate}[leftmargin=*,align=right]
          \item \ifthenelse{\boolean{VG@doc}}{%
              La documentation est permise pour cet examen.}{%
              Aucune documentation n'est permise pour cet examen.}
          \item Les seules calculatrices permises sont les modèles suivants:
            TI BA--35, TI BA II Plus, TI BA II Plus Professional,
            TI--30X/TI--30Xa, TI--30X II (IIS ou IIB), TI-30X MultiView
            (XS ou XB).
          \item Le nombre de points attribué à un problème, ou à une
            partie de problème, est indiqué entre crochets dans la
            marge de droite.
          \item Des points seront attribués aux réponses détaillées et
            justifiées seulement.
          \item Répondre dans ce cahier dans les espaces prévus à cet
            effet. Des pages additionnelles sont disponibles à la fin du
            cahier.
          \item Apposer vos initiales au bas de chaque page du cahier
            dans l'espace réservé à cette fin.
          \item \textbf{Ne pas dégrafer les feuilles, y compris les
              pages additionnelles.}
          \end{enumerate}
        \end{minipage}
      \fi
    \end{center}
  \end{coverpages}
  \stepcounter{page}}

%%%
%%% Configuration de la classe exam
%%%

%% Disposition sur la page
\extrawidth{-1in}               % augmenter les marges gauche et droite
\extraheadheight{0.5in}         % augmenter la marge supérieure

%% Affichage du nombre de points
\bracketedpoints                % entre crochets
\pointsinrightmargin            % dans marge de droite
\setlength{\rightpointsmargin}{0.75in} % distance du bord

%% Styles des folios et des solutions
\renewcommand{\questionlabel}{\bfseries\thequestion.}
\renewcommand{\partlabel}{\thepartno)}
\renewcommand{\solutiontitle}{}

%% Tableau du nombre de points
\vsword{Résultat}               % plutôt que "Score"
\vtword{\textbf{Total}}         % ajout du gras

%% Entête et pied de page
\header{\sffamily\VG@coursenumber}{}{\sffamily\@date}
\headrule
\lfoot{}
\cfoot{\thepage\ de \numpages}
\rfoot{\sffamily\textbf{Initiales}\;\fbox{\strut\hspace{10mm}}}
\pagestyle{headandfoot}

\endinput
