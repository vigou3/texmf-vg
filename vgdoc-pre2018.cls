%%    Modifications à la classe memoir pour avoir l'entête de mes
%%    documents.
%%
%%    Nouvelles commandes:
%%
%%    \coursenumber{}   numéro du cours (obligatoire)
%%    \coursename{}     titre du cours (obligatoire)
%%    \title{}          titre du document (facultatif)
%%    \reponses         pour créer une nouvelle section pour les réponses
%%
%%    Préparé par Vincent Goulet 11 décembre 2003
%%    Révisé  par Vincent Goulet 15 mai 2008
%%    Révisé  par Vincent Goulet 26 mai 2009 pour babel v. 2.0
%%    Révisé  par Vincent Goulet 2 février 2012 pour changer les polices
%%    Révisé  par Vincent Goulet 9 février 2014 pour support de
%%      XeLaTeX et ajout de l'option 'nocc' pour supprimer la notice
%%      de copyright
%%    Révisé par Vincent Goulet janvier et février 2016 pour
%%      utilisation de Lucida Grande Mono DK comme police non
%%      proportionnelle et chargement de Sweave avec modification des
%%      environnements

\NeedsTeXFormat{LaTeX2e}[1995/12/01]
\ProvidesClass{vgdoc}[2016/02/07 (Vincent Goulet)]
\RequirePackage{ifthen}
\RequirePackage{ifxetex}
\newboolean{VG@ccnotice}
\setboolean{VG@ccnotice}{true}
\DeclareOption{nocc}{\setboolean{VG@ccnotice}{false}}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{memoir}}
\ProcessOptions
\LoadClass[11pt,letterpaper,oneside,article]{memoir}

\RequirePackage{natbib}
\RequirePackage[english,french]{babel}
\RequirePackage[autolanguage]{numprint}
\RequirePackage{amsmath}
\RequirePackage{textcomp}
\RequirePackage{url}
\RequirePackage{framed}
\RequirePackage[shortlabels]{enumitem}
\RequirePackage[noae]{Sweave}
\RequirePackage{graphicx}
\RequirePackage{answers}

%% Polices de caractères.
\ifxetex
  \RequirePackage{fontspec}
  \RequirePackage{unicode-math}
  \defaultfontfeatures{Scale=0.92}
  \setmainfont[Ligatures=TeX,Numbers=OldStyle]{Lucida Bright OT}
  \setmathfont{Lucida Bright Math OT}
  \setmonofont{Lucida Grande Mono DK}
  \setsansfont[Scale=1.0,Numbers=OldStyle,
               BoldFont=Myriad Pro Semibold,
               BoldItalicFont=Myriad Pro Semibold Italic]{Myriad Pro}
\else
  \RequirePackage[T1]{fontenc}
  \RequirePackage[utf8]{inputenc}
  \RequirePackage{lucidabr}
  \RequirePackage[scaled=0.92]{beramono}
  \renewcommand{\sfdefault}{Myriad-LF}
\fi
\RequirePackage{microtype}
\RequirePackage{icomma}

%% Couleurs
\RequirePackage[x11names]{xcolor}
\definecolor{comments}{rgb}{0.7,0,0}
\definecolor{link}{rgb}{0,0.4,0.6}        % liens internes
\definecolor{url}{rgb}{0.6,0,0}           % liens externes
\definecolor{citation}{rgb}{0,0.5,0}      % citations
\definecolor{codebg}{named}{LightYellow1} % arrière-plan code R

%% Hyperliens
\RequirePackage{hyperref}
\hypersetup{colorlinks,
  urlcolor=url, linkcolor=link, citecolor=citation,
  pdfauthor={Vincent Goulet}}
\ifxetex
  \setlength{\XeTeXLinkMargin}{1pt} % pour image seule en hyperlien
\fi
  \addto\extrasfrench{%
    \def\definitionautorefname{définition}%
    \def\figureautorefname{figure}%
    \def\exempleautorefname{exemple}%
    \def\exerciceautorefname{exercice}%
    \def\tableautorefname{tableau}%
    \def\thmautorefname{théorème}%
  }

%% Commandes additionnelle pour spécifier le numéro de cours et al.
\newcommand{\VG@coursenumber}{}
\newcommand{\VG@coursename}{}
\newcommand{\VG@title}{}
\newcommand{\coursenumber}[1]{%
  \renewcommand{\VG@coursenumber}{\MakeUppercase{#1}}}
\newcommand{\coursename}[1]{\renewcommand{\VG@coursename}{#1}}
\renewcommand{\title}[1]{%
  \renewcommand{\VG@title}{#1}
  \protected@xdef\thetitle{#1}}

%% Options de babel
\frenchbsetup{CompactItemize=false,%
  ThinSpaceInFrenchNumbers=true}
\addto\captionsfrench{\def\figurename{{\scshape Fig.}}}
\addto\captionsfrench{\def\tablename{{\scshape Tab.}}}

%% Listes. Paramétrage avec enumitem.
\setlist[enumerate]{leftmargin=*,align=left}
\setlist[enumerate,2]{label=\alph*),labelsep=*,leftmargin=1.5em}
\setlist[enumerate,3]{label=\roman*),labelsep=0.5em,align=right}
\setlist[itemize]{leftmargin=*,align=left}

%% Message de contrat de publication à insérer à la fin du document
\ifthenelse{\boolean{VG@ccnotice}}{%
  \AtEndDocument{\vfill\footnotesize\noindent%
    \raisebox{-2pt}{\href{http://creativecommons.org}{%
        \includegraphics[height=10pt,keepaspectratio=true]{by-sa}}} %
    {\textcopyright} Vincent Goulet. Ce document est publié selon le contrat
    \href{http://creativecommons.org/licenses/by-sa/4.0/deed.fr}{%
      Attribution-Partage dans les mêmes conditions 4.0
      International}.}}{}

%% Il faut charger le package lastpage après la commande précédente.
\RequirePackage{lastpage}

%% Numéro de page au centre en bas
\makeoddfoot{plain}{}{\thepage\ de \pageref*{LastPage}}{}
\pagestyle{plain}

%% Ajustement des notes marginales pour alignement avec le logo
\setlength{\marginparsep}{7mm}
\setlength{\marginparwidth}{13mm}
\newlength{\VG@coursetitlewidth}
\newcommand{\VG@measurewidth}{%
  \fontsize{8}{9}
  \ifxetex
    \fontspec{Helvetica Neue LT Std 57 Condensed}
  \else
    \fontfamily{phv}
  \fi
  \settowidth{\VG@coursetitlewidth}{\VG@coursename}}

%% Logo et nom du cours en entête
\renewcommand{\maketitle}{{\flushleft%
    \VG@measurewidth
    \raisebox{4mm}[0mm]{%
      \begin{minipage}[b]{\textwidth}
        \hspace{-18mm}\includegraphics[width=143bp,height=73bp,keepaspectratio=true]{entete}
        \hfill
        \raisebox{1.8mm}{%
          \begin{minipage}[b]{\VG@coursetitlewidth}
            \fontsize{8}{9}
            \ifxetex
              \fontspec{Helvetica Neue LT Std 57 Condensed}
            \else
              \fontfamily{phv}\selectfont
            \fi
            \textbf{\VG@coursenumber} \\
            \VG@coursename
          \end{minipage}}
        \end{minipage}}} \\
  \ifthenelse{\equal{\VG@title}{}}{\vspace{18pt}}{%
    \begin{center} \bfseries \sffamily\VG@title \end{center} \vspace{12pt}}}

%%% Style des titres et numérotation des sous-sections
\chapterstyle{article}
\renewcommand{\chaptitlefont}{\normalfont\Large\sffamily\bfseries\raggedright}
\setsecheadstyle{\normalfont\large\sffamily\bfseries\raggedright}
\setsubsecheadstyle{\normalfont\sffamily\bfseries\raggedright}
\maxsecnumdepth{subsection}

%% Environnements de Sweave
\DefineVerbatimEnvironment{Sinput}{Verbatim}{}
\DefineVerbatimEnvironment{Soutput}{Verbatim}{}
\fvset{listparameters={\setlength{\topsep}{0pt}}}
\renewenvironment{Schunk}{%
  \setlength{\topsep}{0pt}\upshape
  \colorlet{shadecolor}{LightYellow1}
  \begin{snugshade*}}%
  {\end{snugshade*}}

%% Paramètres pour le package answers
\Newassociation{sol}{solution}{solutions}
\Newassociation{rep}{reponse}{reponses}
\newcounter{exercice}[chapter]
\newenvironment{exercice}{%
  \begin{list}{\bfseries \arabic{exercice}.}{%
      \refstepcounter{exercice}
      \settowidth{\labelwidth}{\bfseries \arabic{exercice}.}
      \setlength{\leftmargin}{\labelwidth}
      \addtolength{\leftmargin}{\labelsep}
      \setlist[enumerate,1]{label=\alph*),labelsep=*,leftmargin=1.5em}
      \setlist[enumerate,2]{label=\roman*),labelsep=0.5em,align=right}}
  \item}
  {\end{list}}
\renewenvironment{reponse}[1]{%
  \begin{enumerate}[label=\textbf{#1}.]
  \item}
  {\end{enumerate}}
\renewcommand{\reponseparams}{{\theexercice}}
\renewenvironment{solution}[1]{%
  \begin{enumerate}[label=\textbf{#1}.]
  \item}
  {\end{enumerate}}
\renewcommand{\solutionparams}{{\theexercice}}

\endinput
%%
%% End of file `vgdoc.cls'.
