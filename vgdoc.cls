%%    Modifications à la classe memoir pour avoir l'entête de mes
%%    documents.
%%
%%    Nouvelles commandes:
%%
%%    \coursenumber{}   numéro du cours (obligatoire)
%%    \coursename{}     titre du cours (obligatoire)
%%    \title{}          titre du document (facultatif)
%%    \reponses         pour créer une nouvelle section pour les réponses
%%
%%    Préparé par Vincent Goulet 11 décembre 2003
%%    Révisé  par Vincent Goulet 15 mai 2008
%%    Révisé  par Vincent Goulet 26 mai 2009 pour babel v. 2.0
%%    Révisé  par Vincent Goulet 2 février 2012 pour changer les polices
%%    Révisé  par Vincent Goulet 9 février 2014 pour support de
%%      XeLaTeX et ajout de l'option 'nocc' pour supprimer la notice
%%      de copyright
%%    Révisé par Vincent Goulet janvier et février 2016 pour
%%      utilisation de Lucida Grande Mono DK comme police non
%%      proportionnelle et chargement de Sweave avec modification des
%%      environnements
%%    Révisé par Vincent Goulet novembre 2018 pour conformité avec
%%      les nouvelles normes de présentation visuelle de l'UL (logo et
%%      police Overpass)
%%    Révisé par Vincent Goulet décembre 2023 pour ajouter la commande
%%      intelligente \code tirée de cjs-rcs-article et de la classe du
%%      JSS.
%%    Révisé par Vincent Goulet août 2024 pour modifier les dimensions
%%      de la zone de texte.
%%    Révisé par Vincent Goulet novembre 2024 pour désactiver le
%%      niveau de section \chapter et adapter en conséquence \appendix
%%      et la bibliographie.
%%    Révisé par Vincent Goulet mars 2025 pour ajouter l'option
%%      'sectionbib' au paquetage natbib.
%%

\NeedsTeXFormat{LaTeX2e}[1995/12/01]
\ProvidesClass{vgdoc}[2023/12/21 (Vincent Goulet)]
\RequirePackage{ifthen}
\RequirePackage{ifxetex}
\newboolean{VG@ccnotice}
\setboolean{VG@ccnotice}{true}
\DeclareOption{nocc}{\setboolean{VG@ccnotice}{false}}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{memoir}}
\ProcessOptions
\LoadClass[letterpaper,11pt,oneside,article]{memoir}

\RequirePackage[round,semicolon,authoryear,sectionbib]{natbib}
\RequirePackage[english,french]{babel}
\RequirePackage[autolanguage,np]{numprint}
\RequirePackage{url}
\RequirePackage{amsmath}
\RequirePackage[noae]{Sweave}
\RequirePackage{graphicx}
\RequirePackage[absolute]{textpos}
\RequirePackage{framed}
\RequirePackage[shortlabels]{enumitem}
\RequirePackage{answers}
\RequirePackage[scale=0.92]{ccicons}

%% Marges, entêtes et pieds de page (inspiré des dimensions du manuel
%% de memoir)
\settypeblocksize{8in}{33pc}{*} % hauteur augmentée de 0.25in
\setulmargins{3.5cm}{*}{*}
\setlrmargins{*}{*}{1}
\setheadfoot{\onelineskip}{2\onelineskip}
\setheaderspaces{*}{2\onelineskip}{*}
\checkandfixthelayout

%% Polices de caractères.
\ifxetex
  \RequirePackage{fontspec}
  \RequirePackage{unicode-math}
  \defaultfontfeatures
  {
    Scale = 0.92
  }
  \setmainfont{Lucida Bright OT}
  [
    Ligatures = TeX,
    Numbers = OldStyle
  ]
  \newfontfamily\lucidalining{Lucida Bright OT}
  [
    Ligatures = TeX,
    RawFeature = -onum,
  ]
  \setmathfont{Lucida Bright Math OT}
  \setmonofont{Lucida Grande Mono DK}
  \setsansfont{FiraSans}
  [
    Extension = .otf,
    UprightFont = *-Book,
    BoldFont = *-Medium,
    BoldItalicFont = *-MediumItalic,
    Scale = 1.0,
    Numbers = OldStyle
  ]
  \newfontfamily\fullcaps{FiraSans}
  [
    Extension = .otf,
    UprightFont = *-Book,
    Scale = 1.0,
    Numbers = Uppercase
  ]
  \newfontfamily\Overpass{Overpass Light}
  [
    BoldFont = {Overpass Regular}
  ]
\else
  \RequirePackage[T1]{fontenc}
  \RequirePackage[utf8]{inputenc}
  \RequirePackage{lucidabr}
  \RequirePackage[sfdefault]{FiraSans}
  \RequirePackage[scaled=0.92]{beramono}
\fi
\RequirePackage[babel=true]{microtype}
\RequirePackage{icomma}

%% Couleurs
\RequirePackage[x11names]{xcolor}
\definecolor{comments}{rgb}{0.5,0.55,0.6} % commentaires
\definecolor{link}{rgb}{0,0.4,0.6}        % liens internes
\definecolor{url}{rgb}{0.6,0,0}           % liens externes
\definecolor{citation}{rgb}{0,0.5,0}      % citations
\definecolor{codebg}{named}{LightYellow1} % arrière-plan code R
\definecolor{darkgreen}{rgb}{0,0.6,0}      % rouge bandeau UL

%% Hyperliens
\AtEndPreamble{%
  \RequirePackage{bookmark}     % charge hyperref
  \hypersetup{%
    pdfauthor={Vincent Goulet},
    colorlinks = true,
    urlcolor = {url},
    linkcolor = {link},
    citecolor = {citation}}
  \bookmarksetup{%
    open = true,
    depth = 3,
    numbered = true}
  \ifxetex
    \setlength{\XeTeXLinkMargin}{1pt} % pour image seule en hyperlien
  \fi
  \addto\extrasfrench{%
    \def\subsectionautorefname{section}%
    \def\subsubsectionautorefname{section}%
    \def\definitionautorefname{définition}%
    \def\figureautorefname{figure}%
    \def\exempleautorefname{exemple}%
    \def\exerciceautorefname{exercice}%
    \def\tableautorefname{tableau}%
    \def\thmautorefname{théorème}}}

%% Commandes additionnelle pour spécifier le numéro de cours et al.
\newcommand{\VG@coursenumber}{}
\newcommand{\VG@coursename}{}
\newcommand{\VG@title}{}
\newcommand{\coursenumber}[1]{%
  \renewcommand{\VG@coursenumber}{\MakeUppercase{#1}}}
\newcommand{\coursename}[1]{\renewcommand{\VG@coursename}{#1}}
\renewcommand{\title}[1]{%
  \renewcommand{\VG@title}{#1}
  \protected@xdef\thetitle{#1}}

%% Options de babel
\frenchbsetup{CompactItemize=false,%
  ThinSpaceInFrenchNumbers=true,
  og=«, fg=»}
\addto\captionsfrench{\def\figurename{{\scshape Fig.}}}
\addto\captionsfrench{\def\tablename{{\scshape Tab.}}}

%% Listes. Paramétrage avec enumitem.
\setlist[enumerate]{leftmargin=*,align=left}
\setlist[enumerate,2]{label=\alph*),labelsep=*,leftmargin=1.5em}
\setlist[enumerate,3]{label=\roman*),labelsep=0.5em,align=right}
\setlist[itemize]{leftmargin=*,align=left}

%% Message de contrat de publication à insérer à la fin du document
\ifthenelse{\boolean{VG@ccnotice}}{%
  \AtEndDocument{%
    \vfill\noindent\parbox{\linewidth}{%
      \footnotesize%
      \raisebox{0.8pt}{\ccbysa} %
      \raisebox{0.8pt}{\ccCopy} %
      Vincent Goulet. Ce document est mis à disposition sous licence
      \href{http://creativecommons.org/licenses/by-sa/4.0/deed.fr}{%
        Attribution-Partage dans les mêmes conditions 4.0
        International} de Creative Commons.}}}{}

%% Numéro de page au centre en bas
\makeoddfoot{plain}{}{\thepage\ de \thelastpage}{}
\pagestyle{plain}

%% Positionnement du logo
\TPGrid{8}{64}
\newlength{\logoheight}
\newlength{\logowidth}
\setlength{\logoheight}{4\TPVertModule}
\setlength{\logowidth}{4.727273\logoheight}

%% Logo et nom du cours en entête
\renewcommand{\maketitle}{
  \begin{textblock*}{165mm}(25mm,15mm)
    \setlength{\parindent}{0mm}
    \includegraphics[height=\logoheight,keepaspectratio]{ul_p}
    \raisebox{8.5mm}{%
      \hspace*{5mm}
      \rule[-4.6mm]{0.3pt}{11.1mm}%
      \hspace*{2.5mm}
      \begin{minipage}{65mm}
        \ifxetex \Overpass \else \fontfamily{phv} \fi
        \fontsize{10}{11}
        \textbf{\VG@coursenumber} \\
        \textcolor[gray]{0.3}{\VG@coursename}
      \end{minipage}}
  \end{textblock*}
  \mbox{} % provoquer un déplacement vertical
  \ifthenelse{\equal{\VG@title}{}}{\vspace{18pt}}{%
    \begin{center}
      \bfseries \sffamily\VG@title \end{center} \vspace{12pt}}}

%%% Style des titres et numérotation des sous-sections (le
%%% niveau \chapter est désactivé)
\let\chapter\relax
\counterwithout{section}{chapter}
\setsecheadstyle{\normalfont\Large\sffamily\bfseries\raggedright}
\setsubsecheadstyle{\normalfont\large\sffamily\bfseries\raggedright}
\setsubsubsecheadstyle{\normalfont\sffamily\bfseries\raggedright}
\setsecnumdepth{subsubsection}
\maxsecnumdepth{subsubsection}

%%% Redéfinition de \appendix pour fonctionner avec des sections
%%% plutôt qu'avec des chapitres
\renewcommand*\appendix{\par
  \setcounter{section}{0}
  \gdef\thesection{\@Alph\c@section}}

%%% Idem pour la bibliographie
\renewcommand{\@memb@bsec}{%
  \section*{\bibname}%
  \addcontentsline{toc}{section}{\bibname}%
  \prebibhook}

%% Environnements de Sweave
\DefineVerbatimEnvironment{Sinput}{Verbatim}{}
\DefineVerbatimEnvironment{Soutput}{Verbatim}{}
\fvset{listparameters={\setlength{\topsep}{0pt}}}
\renewenvironment{Schunk}[1][\linewidth]{%
  \setlength{\topsep}{1pt}
  \def\FrameCommand##1{\hskip\@totalleftmargin
     \vrule width 2pt\colorbox{codebg}{\hspace{3pt}##1}%
    % There is no \@totalrightmargin, so:
    \hskip-#1 \hskip-\@totalleftmargin \hskip\columnwidth}%
  \MakeFramed {\advance\hsize-\width
    \@totalleftmargin\z@ \linewidth\hsize
    \advance\labelsep\fboxsep
    \@setminipage}%
}{\par\unskip\@minipagefalse\endMakeFramed}

%% Paramètres pour le package answers
\Newassociation{sol}{solution}{solutions}
\Newassociation{rep}{reponse}{reponses}
\newcounter{exercice}[chapter]
\newenvironment{exercice}{%
  \begin{list}{\bfseries \arabic{exercice}.}{%
      \refstepcounter{exercice}
      \settowidth{\labelwidth}{\bfseries \arabic{exercice}.}
      \setlength{\leftmargin}{\labelwidth}
      \addtolength{\leftmargin}{\labelsep}
      \setlist[enumerate,1]{label=\alph*),labelsep=*,leftmargin=1.5em}
      \setlist[enumerate,2]{label=\roman*),labelsep=0.5em,align=right}}
  \item}
  {\end{list}}
\renewenvironment{reponse}[1]{%
  \begin{enumerate}[label=\textbf{#1}.]
  \item}
  {\end{enumerate}}
\renewcommand{\reponseparams}{{\theexercice}}
\renewenvironment{solution}[1]{%
  \begin{enumerate}[label=\textbf{#1}.]
  \item}
  {\end{enumerate}}
\renewcommand{\solutionparams}{{\theexercice}}

% Version futée d'une commande pour composer des extraits de code  qui
% désactive les caractères spéciaux «_», «~» et «$». Tirée de la
% classe du Journal of Statistical Software.
\newcommand\code{\bgroup\@makeother\_\@makeother\~\@makeother\$\code@}
\def\code@#1{{\normalfont\ttfamily\hyphenchar\font=-1 #1}\egroup}

\endinput
%%
%% End of file `vgdoc.cls'.
