%%    Modifications à la classe exam pour mes examens.
%%
%%    Nouvelles commandes:
%%
%%    \coursenumber{}   numéro du cours (obligatoire)
%%    \exam{}           examen (ogligatoire)
%%    \date{}           date (ogligatoire)
%%    \duration{}       durée (ogligatoire)
%%
%%    Préparé par Vincent Goulet 9 mars 2014
%%    Révisé par Vincent Goulet mars 2016
%%      pour utilisation de Lucida Grande Mono DK comme
%%      police non proportionnelle
%%    Révisé par Vincent Goulet novembre 2018
%%      pour conformité avec les nouvelles normes de
%%      présentation visuelle de l'UL (logo et police
%%      Overpass)
%%    Révisé par Vincent Goulet en octobre 2020
%%      pour ajouter des emplacements pour le nom et le NI sur la
%%      couverture pour cause de COVID-19 et d'utilisation de
%%      Gradescope
%%    Révisé par Vincent Goulet en avril 2022
%%      pour ajouter des options et des commandes pour divers
%%      éléments de mise en page afin de permettre la production
%%      de cahiers de réponses et de solutions séparés du
%%      questionnaire
%%    Révisé par Vincent Goulet en décembre 2022
%%      pour modifier le style des cercles pour les questions
%%      booléennes
%%    Révisé par Vincent Goulet en octobre 2023
%%      pour préciser, dans les consignes, que seules les questions à
%%      développement sont visées par l'exigence de réponses
%%      justifiées.
%%    Révisé par Vincent Goulet août 2024 pour augmenter les dimensions
%%      de la zone de texte.
%%    Révisé par Vincent Goulet mars 2025 pour ajouter l'option 'np' au
%%      paquetage numprint.
%%
\NeedsTeXFormat{LaTeX2e}[1995/12/01]
\ProvidesClass{vgexam}[2022/04/26 (Vincent Goulet)]
\RequirePackage{ifxetex}
\newif\ifVG@questionsbook \VG@questionsbooktrue  % questionnaire?
\newif\ifVG@answersbook   \VG@answersbookfalse   % cahier de réponses?
\newif\ifVG@solutionsbook \VG@solutionsbookfalse % cahier de solutions?
\newif\ifVG@doc           \VG@docfalse           % documentation permise?
\DeclareOption{questionsbook}{%
  \VG@questionsbooktrue}
\DeclareOption{answersbook}{%
  \VG@answersbooktrue
  \VG@solutionsbookfalse
  \VG@questionsbookfalse}
\DeclareOption{solutionsbook}{%
  \VG@solutionsbooktrue
  \VG@answersbookfalse
  \VG@questionsbookfalse}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{exam}}
\ProcessOptions\relax
\LoadClass[11pt,addpoints]{exam}

%% Activer l'option 'answers' de exam si l'option 'solutionsbook' est
%% spécifiée.
\ifVG@solutionsbook
  \printanswerstrue
\fi

%% Charger les autres paquetages requis par défaut.
\RequirePackage[french]{babel}
\RequirePackage[autolanguage,np]{numprint}
\RequirePackage{amsmath}
\RequirePackage{graphicx}
\RequirePackage[x11names]{xcolor}
\RequirePackage[absolute]{textpos}
\RequirePackage[shortlabels]{enumitem}
\RequirePackage{answers}

%% Polices de caractères.
\ifxetex
  \RequirePackage{fontspec}
  \RequirePackage{unicode-math}
  \defaultfontfeatures{Scale=0.92}
  \setmainfont[Ligatures=TeX]{Lucida Bright OT}
  \setmathfont{Lucida Bright Math OT}
  \setmonofont{Lucida Grande Mono DK}
  \setsansfont[BoldFont = {Overpass SemiBold}]{Overpass Regular}
\else
  \RequirePackage[T1]{fontenc}
  \RequirePackage[utf8]{inputenc}
  \RequirePackage{lucidabr}
  \RequirePackage[scaled=0.92]{beramono}
\fi
\RequirePackage[babel=true]{microtype}
\RequirePackage{icomma}

%% Commandes additionnelles pour spécifier le numéro de cours et al.
\newcommand*{\VG@coursenumber}{}
\newcommand*{\VG@exam}{}
\newcommand*{\VG@duration}{}
\newcommand*{\VG@date}{}
\newcommand{\coursenumber}[1]{%
  \renewcommand{\VG@coursenumber}{\MakeUppercase{#1}}
  \protected@xdef\thecoursenumber{\MakeUppercase{#1}}}
\newcommand{\exam}[1]{%
  \renewcommand{\VG@exam}{#1}
  \protected@xdef\theexam{#1}}
\newcommand{\duration}[1]{%
  \renewcommand{\VG@duration}{#1}
  \protected@xdef\theduration{#1}}
\renewcommand{\date}[1]{%
  \renewcommand{\VG@date}{#1}
  \protected@xdef\thedate{#1}}
\newcommand{\examwithdoc}{\VG@doctrue}

%% Options de babel.
\frenchbsetup{%
  CompactItemize=false,%
  ThinSpaceInFrenchNumbers=true,
  og=«, fg=»}
\addto\captionsfrench{\def\figurename{{\scshape Fig.}}}
\addto\captionsfrench{\def\tablename{{\scshape Tab.}}}

%% Dimensions pour le positionnement du logo.
\TPGrid{8}{64}
\newlength{\logoheight}
\newlength{\logowidth}
\setlength{\logoheight}{4\TPVertModule}
\setlength{\logowidth}{4.727273\logoheight}

%% Redéfinition de la commande \maketitle pour créer la page de titre
%% appropriée selon le type de document (questionnaire, cahier de
%% réponses, cahier de solutions).
\renewcommand{\maketitle}{%
  \begin{coverpages}
    \flushleft\sffamily%
    \pagestyle{empty}
    \begin{textblock*}{165mm}(25mm,15mm)
      \includegraphics[height=\logoheight,keepaspectratio]{UL_FSG_Actuariat_N_d-2L}
    \end{textblock*}
    \vspace*{35mm}
    \begin{center}
      \renewcommand{\arraystretch}{1.2}
      \begin{tabular*}{115mm}{@{}p{70mm}p{35mm}@{}}
        \textbf{Cours} \newline \thecoursenumber &
        \textbf{Examen} \newline \theexam \\
        \textbf{Professeur} \newline Vincent Goulet &
        \textbf{Date} \newline \thedate \\
        \textbf{Nombre total de pages} \newline \numpages &
        \textbf{Durée} \newline \theduration \\
      \end{tabular*}\par
      \vspace{20mm}
      \ifVG@questionsbook
        \bgroup
          \setlength{\fboxsep}{2mm}
          \fbox{QUESTIONNAIRE}\par
        \egroup
      \fi
      \ifVG@answersbook
        \begin{minipage}{115mm}
          \setlength{\fboxsep}{2mm}
          NOM\hfill  \fbox{\strut\hspace{100mm}} \\[5mm]
          NUMÉRO D'IDENTIFICATION (NI)\hfill \fbox{\strut\hspace{55mm}}
        \end{minipage}\par
      \fi
      \ifVG@solutionsbook
        \bgroup
          \setlength{\fboxsep}{2mm}
          \fbox{SOLUTIONS}
        \egroup
      \else
        \vspace{20mm}
        INSTRUCTIONS \par
        \vspace{10mm}
        \begin{minipage}{11.5cm}
          \begin{enumerate}[leftmargin=*,align=right]
          \ifVG@questionsbook
            \item \textbf{CE CAHIER NE SERA PAS CORRIGÉ.}
            \item
              \ifVG@doc
                La documentation est permise pour cet examen.
              \else
                Aucune documentation n'est permise pour cet examen.
              \fi
            \item Les seules calculatrices permises sont celles
              approuvées par la Faculté des sciences et de génie.
            \item Le nombre de points attribué à un problème est indiqué
              entre crochets dans la marge de droite.
            \item Pour les questions à développement, des points
              seront attribués aux réponses détaillées et justifiées
              seulement.
            \item Ne pas dégrafer les feuilles.
          \fi
          \ifVG@answersbook
            \item Répondre dans ce cahier dans les espaces prévus à cet
              effet. Des pages additionnelles sont disponibles à la fin du
              cahier.
            \item Apposer vos initiales au bas de chaque page du cahier
              dans l'espace réservé à cette fin.
            \item Ne pas dégrafer les feuilles, y compris les
              pages additionnelles.
          \fi
          \end{enumerate}
        \end{minipage}
      \fi
    \end{center}
  \end{coverpages}
  \stepcounter{page}
  \newpage
  \ifVG@questionsbook\else
    \renewcommand\choicelabel{\thechoice}
  \fi
  \ifVG@answersbook
    \rfoot{\sffamily\textbf{Initiales}\;\fbox{\strut\hspace{10mm}}}
  \fi
}

%% Commande pour ajouter des pages additionnelles en fin de cahier
\newcommand{\extrablankpage}{%
  \newpage
  \extraheadheight{-.1in}
  \header{}{\sffamily\small ESPACE ADDITIONNEL}{}
    \vspace*{\stretch{1}}}

%%%
%%% Configuration de la classe exam
%%%

%% Disposition sur la page
\extrawidth{-0.5in}               % augmenter les marges gauche et droite
\extraheadheight{0.25in}         % augmenter la marge supérieure

%% Affichage du nombre de points
\bracketedpoints                % entre crochets
\pointsinrightmargin            % dans marge de droite
\setlength{\rightpointsmargin}{0.75in} % distance du bord

%% Affichage des points bonis
\marginbonuspointname{}
\bonuspointformat{[$+$\themarginpoints]}

%% Styles des folios et des solutions
\renewcommand{\questionlabel}{\bfseries\thequestion.}
\renewcommand{\partlabel}{\thepartno)}
\renewcommand{\solutiontitle}{}

%% Style des «boites» (des cercles, en fait) pour les questions
%% booléennes et leurs réponses
\setlength{\unitlength}{1mm}
\newsavebox{\VG@circ}
\newsavebox{\VG@circfilled}
\savebox{\VG@circ}(2.5,2.5){\put(1.25,0){\circle{2.5}}}
\savebox{\VG@circfilled}(2.5,2.5){\put(1.25,0){\circle*{2.5}}}
\checkboxchar{\usebox{\VG@circ}}
\checkedchar{\usebox{\VG@circfilled}}

%% Entête et pied de page
\header{\sffamily\VG@coursenumber}{}{\sffamily\thedate}
\headrule
\lfoot{}
\cfoot{\thepage\ de \numpages}
\pagestyle{headandfoot}

\endinput
