%%    Modifications à la classe exam pour mes examens.
%%
%%    Nouvelles commandes:
%%
%%    \coursenumber{}   numéro du cours (obligatoire)
%%    \exam{}           examen (ogligatoire)
%%    \date{}           date (ogligatoire)
%%    \duration{}       durée (ogligatoire)
%%
%%    Préparé par Vincent Goulet 9 mars 2014
%%    Révisé par Vincent Goulet mars 2016
%%      pour utilisation de Lucida Grande Mono DK comme
%%      police non proportionnelle
%%    Révisé par Vincent Goulet novembre 2018
%%      pour conformité avec les nouvelles normes de
%%      présentation visuelle de l'UL (logo et police
%%      Overpass)

\NeedsTeXFormat{LaTeX2e}[1995/12/01]
\ProvidesClass{vgexam}[2016/03/07 (Vincent Goulet)]
\RequirePackage{ifthen}
\RequirePackage{ifxetex}
\newboolean{VG@doc}
\newboolean{VG@nodoc}
\setboolean{VG@doc}{false}
\DeclareOption{nodoc}{\setboolean{VG@doc}{false}}
\DeclareOption{doc}{\setboolean{VG@doc}{true}}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{exam}}
\ProcessOptions
\LoadClass[11pt,addpoints]{exam}

\RequirePackage{ifthen}
\RequirePackage[french]{babel}
\RequirePackage[autolanguage]{numprint}
\RequirePackage{amsmath}
\RequirePackage[noae]{Sweave}
\RequirePackage{graphicx}
\RequirePackage[absolute]{textpos}

%% Polices de caractères.
\ifxetex
  \RequirePackage{fontspec}
  \RequirePackage{unicode-math}
  \defaultfontfeatures{Scale=0.92,Ligatures=TeX}
  \setmainfont{Lucida Bright OT}
  \setmathfont{Lucida Bright Math OT}
  \setmonofont{Lucida Grande Mono DK}
  \setsansfont[BoldFont = {Overpass SemiBold}]{Overpass Regular}
\else
  \RequirePackage[T1]{fontenc}
  \RequirePackage[utf8]{inputenc}
  \RequirePackage{lucidabr}
  \RequirePackage[scaled=0.92]{beramono}
\fi
\RequirePackage[babel=true]{microtype}
\RequirePackage{icomma}

%% Commandes additionnelles pour spécifier le numéro de cours et al.
\newcommand{\VG@coursenumber}{}
\newcommand{\VG@exam}{}
\newcommand{\VG@duration}{}
\newcommand{\coursenumber}[1]{%
  \renewcommand{\VG@coursenumber}{\MakeUppercase{#1}}}
\newcommand{\exam}[1]{\renewcommand{\VG@exam}{#1}}
\newcommand{\duration}[1]{\renewcommand{\VG@duration}{#1}}

%% Commande pour ajouter des pages additionnelles en fin de cahier
\newcommand{\extrablankpage}{%
  \thispagestyle{foot}
  \begin{center}
    \vspace*{-1in} \sffamily\small ESPACE ADDITIONNEL
  \end{center}
  \vspace*{\stretch{1}}
  \newpage}

%% Options de babel
\frenchbsetup{CompactItemize=false,%
  ThinSpaceInFrenchNumbers=true,
  og=«, fg=»}
\addto\captionsfrench{\def\figurename{{\scshape Fig.}}}
\addto\captionsfrench{\def\tablename{{\scshape Tab.}}}

%% Positionnement du logo
\TPGrid{8}{64}
\newlength{\logoheight}
\newlength{\logowidth}
\setlength{\logoheight}{4\TPVertModule}
\setlength{\logowidth}{4.727273\logoheight}

%% Redéfinition de la commande \maketitle pour créer la page titre et
%% son verso qui contient le tableau des points
\renewcommand{\maketitle}{%
  \begin{coverpages}
    \flushleft\sffamily%
    \pagestyle{empty}
    \begin{textblock*}{165mm}(25mm,15mm)
      \includegraphics[height=\logoheight,keepaspectratio]{UL_FSG_Actuariat_N_d-2L}
    \end{textblock*}
    \vspace*{35mm}
    \begin{center}
      \renewcommand{\arraystretch}{1.2}
      \begin{tabular}{p{7cm}p{3.5cm}}
        \textbf{Cours} \newline \VG@coursenumber &
        \textbf{Examen} \newline \VG@exam \\
        \textbf{Professeur} \newline Vincent Goulet &
        \textbf{Date} \newline \@date \\
        \textbf{Nombre total de pages} \newline \totalnumpages &
        \textbf{Durée} \newline \VG@duration \\
      \end{tabular}\par
      \vspace{20mm}
      \ifprintanswers
        {\large \textbf{SOLUTIONS}} \\[\baselineskip]
        \textbf{À L'USAGE EXCLUSIF DE L'AUXILIAIRE D'ENSEIGNEMENT}
        \vfill
      \else
        INSTRUCTIONS \par
        \vspace{10mm}
        \begin{minipage}{11.5cm}
          \begin{enumerate}
          \item \ifthenelse{\boolean{VG@doc}}{%
              La documentation est permise pour cet examen.}{%
              Aucune documentation n'est permise pour cet examen.}
          \item Les seules calculatrices permises sont les modèles suivants:
            TI BA--35, TI BA II Plus, TI BA II Plus Professional,
            TI--30X/TI--30Xa, TI--30X II (IIS ou IIB), TI-30X MultiView
            (XS ou XB).
          \item Répondre dans ce cahier dans les espaces prévus à cet
            effet. Des pages additionnelles sont disponibles à la fin du
            cahier.
          \item Le nombre de points attribué à un problème est indiqué
            entre crochets dans la marge de droite.
          \item Des points seront attribués aux réponses détaillées et
            justifiées seulement.
          \end{enumerate}
        \end{minipage}
      \fi
    \end{center}
    \newpage
    \pagestyle{headandfoot}
    \begin{center}
      \large%
      RÉSERVÉ AU CORRECTEUR --- NE RIEN INSCRIRE SUR CETTE PAGE
    \end{center}
    \vspace{25mm}
    \begin{center}
      \gradetable
    \end{center}
  \end{coverpages}}

%%%
%%% Configuration de la classe exam
%%%

%% Disposition sur la page
\extrawidth{-1in}               % augmenter les marges gauche et droite
\extraheadheight{0.5in}         % augmenter la marge supérieure

%% Affichage du nombre de points
\bracketedpoints                % entre crochets
\pointsinrightmargin            % dans marge de droite
\setlength{\rightpointsmargin}{0.75in} % distance du bord

%% Styles des folios et des solutions
\renewcommand{\questionlabel}{\bfseries\thequestion.}
\renewcommand{\partlabel}{\thepartno)}
\renewcommand{\solutiontitle}{}

%% Tableau du nombre de points
\vsword{Résultat}               % plutôt que "Score"
\vtword{\textbf{Total}}         % ajout du gras

%% Entête et pied de page
\header{\sffamily\VG@coursenumber}{}{\sffamily\@date}
\headrule
\footer{}{\thepage\ de \numpages}{}
\coverfooter{}{\romannumeral\numcoverpages}{}
\pagestyle{headandfoot}

\endinput
